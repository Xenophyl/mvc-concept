from PyQt4.QtCore import QTimer, QObject, pyqtSignal

from read_in_file import read_in
 
class TaskController(QObject):
 
    notify_progress = pyqtSignal(str)
    notify_item = pyqtSignal(object)
    finish_progress = pyqtSignal()
    fire_label = pyqtSignal(int)
   
    def __init__(self, parent=None):
        QObject.__init__(self, parent)
 
    def start(self):

        #   create a generator object
        self.element = read_in(file_name = 'test.txt')
 
        self.timer = QTimer()
 
        # assoziiert increment() mit TIMEOUT Ereignis
        self.timer.setSingleShot(False)
        self.timer.setInterval(50)
        self.timer.timeout.connect(self.increment)
        self.timer.start()

    def increment(self):
 
        try:
            self.notify_item.emit(next(self.element))
 
        except StopIteration:

            self.finish_progress.emit()
 
            self.timer.stop()
       
    def stop(self):

        self.timer.stop()
