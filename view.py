import sys
 
from PyQt4.QtCore import QTimer, pyqtSignal, \
     QThread, Qt
 
from PyQt4.QtGui import QDialog, QLabel, QPushButton, \
     QApplication, QVBoxLayout, QStandardItem, QStandardItemModel, \
	 QTreeView, QComboBox

from controller import TaskController
        
class MyCustomDialog(QDialog):
 
    finish = pyqtSignal()
 
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
		       
        layout = QVBoxLayout(self)

        self._standard_item_model = QStandardItemModel(0, 1, self)
 
        self.tree = QTreeView(self)
        self.tree.setModel(self._standard_item_model)
        self.tree.sortByColumn(0, Qt.AscendingOrder)
		
        self.label = QLabel(self)
       
        self.pushButton_start = QPushButton("Start", self)
        self.pushButton_stopp = QPushButton("Stopp", self)
        self.pushButton_close = QPushButton("Close", self)
 
        layout.addWidget(self.label)
        layout.addWidget(self.tree)
        layout.addWidget(self.pushButton_start)
        layout.addWidget(self.pushButton_stopp)
        layout.addWidget(self.pushButton_close)

        self.combo_box = QComboBox()
        self.combo_box.setModel(self._standard_item_model)
        self.combo_box.show()
 
        self.pushButton_start.clicked.connect(self.on_start)
        self.pushButton_stopp.clicked.connect(self.on_finish)
        self.pushButton_close.clicked.connect(self.close)
 
    def populate_item(self, i):
        item = [QStandardItem(i)]
        self._standard_item_model.appendRow(item)
 
    def on_label(self, i):
         self.label.setText("Result: {}".format(i))
       
    def on_start(self):
         self._standard_item_model.clear()
         self.label.clear()
         
         task_thread = QThread(self)
         task_thread.work = TaskController()

         task_thread.work.moveToThread(task_thread)
		 
         task_thread.work.fire_label.connect(self.on_label)
         task_thread.work.notify_item.connect(self.populate_item)
         task_thread.work.finish_progress.connect(task_thread.quit)
 
         self.finish.connect(task_thread.work.stop)
 
         task_thread.started.connect(task_thread.work.start)
 
         task_thread.finished.connect(task_thread.deleteLater)
 
         task_thread.start()
 
    def on_finish(self):
         self.finish.emit()
     
def main():
    app = QApplication(sys.argv)
    window = MyCustomDialog()
    window.resize(600, 400)
    window.show()
    sys.exit(app.exec_())
 
if __name__ == "__main__":
    main()
