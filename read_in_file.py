#!/usr/bin/env python
#-*- coding:utf-8 -*-

def read_in(file_name = None):
	'''
	NOTICE
	======
	A generator function to read a (large) file lazily
	'''
	#	open a connection to the file with 'context-manager'
	with open(file_name, 'r') as file_object:

		for text in file_object:

			yield text
